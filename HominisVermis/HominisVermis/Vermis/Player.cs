﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using CDLib;

namespace HominisVermis.Vermis
{
    class Player
    {
        // [DATA]
        Sprite sprite;
        Vector2 position;

        // [METHODS]

        // CONSTRUCTORS
        public Player(Texture2D _texture, Vector2 _position)
        {
            sprite = new Sprite(_texture, _position, new Color(255, 255, 255, 255));
            position = _position;
        }

        public void Draw(SpriteBatch batch)
        {
            sprite.Draw(batch, false);
        }
    }
}
